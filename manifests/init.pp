class bkraft-nelk {

	package {'java-1.7.0-openjdk':
		ensure => present,
	}
	
	exec {'elasticsearchkey':
		command => '/bin/rpm --import http://packages.elasticsearch.org/GPG-KEY-elasticsearch',
	}

	yumrepo{'elasticsearch-1.1':
		ensure	=>	present,
		baseurl	=>	"http://packages.elasticsearch.org/elasticsearch/1.1/centos",
		gpgcheck	=>	1,
		gpgkey	=>	"http://packages.elasticsearch.org/GPG-KEY-elasticsearch",
		enabled	=>	1,
		descr	=>	"logstash repository for 1.1.x packages",
	}
	
	package {'elasticsearch':
		ensure	=>	present,
		require =>	[Package['java-1.7.0-openjdk'], Yumrepo['elasticsearch-1.1'], Exec['elasticsearchkey']],
	}
	
	exec {'elasticsearchconfig':
		command => "/bin/echo \"script.disable_dynamic: true\nnetwork.host: localhost\ndiscovery.zen.ping.multicast.enabled: false\n\" >> /etc/elasticsearch/elasticsearch.yml",
		unless	=>	["/bin/egrep '^discovery.zen.ping.multicast.enabled: false' /etc/elasticsearch/elasticsearch.yml", "/bin/egrep '^network.host: localhost' /etc/elasticsearch/elasticsearch.yml", "/bin/egrep '^script.disable_dynamic: true' /etc/elasticsearch/elasticsearch.yml"],
		require	=>	Package['elasticsearch'],
	}
	
	service{'elasticsearch':
		ensure	=>	running,
		enable	=>	true,
		require	=>	Exec['elasticsearchconfig'],
	}
	
	exec {'kibanadownload':
		command =>	'/usr/bin/curl -q "https://download.elasticsearch.org/kibana/kibana/kibana-3.1.2.tar.gz" -o "/tmp/kibana-3.1.2.tar.gz"',
		creates =>	"/tmp/kibana-3.1.2.tar.gz",
		unless	=>	"/bin/ls -l /tmp/kibana-3.1.2.tar.gz",
		require	=>	File['/usr/share/nginx/kibana3'],
	}
	
	exec{'kibanaunpack':
		command =>	'/bin/tar --no-same-owner -xzf /tmp/kibana-3.1.2.tar.gz -C /usr/share/nginx/kibana3 --strip 1',
		user	=>	nginx,
		require	=>	[Exec['kibanadownload'], Package['nginx']],
	}
	
	package{'epel-release':
		ensure	=>	present,
	}
	
	package{'nginx':
		ensure	=>	present,
		require =>	Package['epel-release'],
	}
	
	file{'/etc/nginx/conf.d/default.conf':
		ensure	=>	present,
		require =>	Package['nginx'],
		source	=>	"puppet:///modules/bkraft-nelk/nginx.conf",
		mode	=>	0644,
		owner	=>	root,
		group	=>	root,
	}
	
	service{'nginx':
		ensure	=>	running,
		enable	=>	true,
		require	=>	File['/etc/nginx/conf.d/default.conf'],
	}
	
	file{'/usr/share/nginx/kibana3':
		ensure	=>	directory,
		owner	=>	nginx,
		group	=>	nginx,
		mode	=>	6755,
	}
	
	exec{'configurekibana':
		command	=>	"/usr/bin/perl -i -pe 's#9200#80#' /usr/share/nginx/kibana3/config.js",
		require =>	Exec['kibanaunpack'],
	}
	
	yumrepo{'logstash-1.4':
                ensure => present,
		baseurl	=> "http://packages.elasticsearch.org/logstash/1.4/centos",
		enabled	=>	1,
		gpgcheck	=>	1,
		gpgkey	=> "http://packages.elasticsearch.org/GPG-KEY-elasticsearch",
		descr	=> "logstash repository for 1.4.x packages",
	}
	
	package{'logstash':
		ensure	=>	present,
		require	=>	Yumrepo['logstash-1.4'],
	}
	
	exec{'logstashpki':
		command	=>	"/usr/bin/openssl req -x509 -batch -nodes -days 3650 -newkey rsa:2048 -keyout /etc/pki/tls/private/logstash-forwarder.key -out /etc/pki/tls/certs/logstash-forwarder.crt",
                require =>      Package['logstash'],
	}
	
	file{'/etc/logstash/conf.d/10-syslog.conf':
	        ensure  =>      present,
	        require =>      Package['logstash'],
	        source  =>      "puppet:///modules/bkraft-nelk/10-syslog.conf",
	        mode    =>      0644,
	        owner   =>      root,
	        group   =>      root,
	}
	file{'/etc/logstash/conf.d/30-lumberjack-output.conf':
	        ensure  =>      present,
	        require =>      Package['logstash'],
	        source  =>      "puppet:///modules/bkraft-nelk/30-lumberjack-output.conf",
	        mode    =>      0644,
	        owner   =>      root,
	        group   =>      root,
	}
	file{'/etc/logstash/conf.d/01-lumberjack-input.conf':
	        ensure  =>      present,
	        require =>      Package['logstash'],
	        source  =>      "puppet:///modules/bkraft-nelk/01-lumberjack-input.conf",
	        mode    =>      0644,
	        owner   =>      root,
	        group   =>      root,
	}
	file{'/etc/logstash/conf.d/02-syslog-input.conf':
	        ensure  =>      present,
	        require =>      Package['logstash'],
	        source  =>      "puppet:///modules/bkraft-nelk/02-syslog-input.conf",
	        mode    =>      0644,
	        owner   =>      root,
	        group   =>      root,
	}

        service{'logstash':
                ensure  =>      running,
                enable  =>      true,
                require =>      [Exec['logstashpki'], File['/etc/logstash/conf.d/10-syslog.conf'], File['/etc/logstash/conf.d/30-lumberjack-output.conf'], File['/etc/logstash/conf.d/01-lumberjack-input.conf'], File['/etc/logstash/conf.d/02-syslog-input.conf']],
        }	

	notify{'Termination Message':
		message	=>	"Installation of the software terminated. Please add the following lines in your firewall configuration:\n-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT\n-A INPUT -m state --state NEW -m tcp -p tcp --dport 1514 -j ACCEPT\n-A INPUT -m state --state NEW -m udp -p udp --dport 1514 -j ACCEPT",
		require	=>	[Service['logstash'], Service['elasticsearch'], Exec['kibanaunpack'], Exec['configurekibana']],
	}
}
